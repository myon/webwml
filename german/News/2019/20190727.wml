<define-tag pagetitle>DebConf19 schließt in Curitiba, Daten der DebConf20 bekanntgegeben</define-tag>

<define-tag release_date>2019-07-27</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="b061a3eb5a284dce5a149a8505b3121bd7e4c6b6" maintainer="Erik Pfannenstein"

<p>
Am heutigen Samstag, dem 27. Juli 2019, kam die jährliche Debian-Entwickler- 
und Unterstützer-Konferenz zu ihrem Ende. Mit mehr als 380 Teilnehmerinnen und 
Teilnehmern aus 50 verschiedenen Ländern, insgesamt 145 Vorträgen, 
Diskussionsrunden, Birds of a Feather (BoF)-Treffen, Worksops und Aktivitäten 
war die 
<a href="https://debconf19.debconf.org">DebConf19</a> ein großer Erfolg.
</p>

<p>
Der Konferenz ging auch dieses Jahr wieder das DebCamp voraus, welches vom 
14. bis zum 19. Juli stattfand und sich auf Einzelarbeiten sowie Team-Sprints 
konzentrierte, während derer die Debian-Entwicklung in persönlicher 
Zusammenarbeit vorangetrieben wurde. Außerdem gab es auf dem DebCamp einen 
dreitägigen Paketierungs-Workshop, der Neuankömmlinge in die Lage versetzte,  Debian-Pakete zu schnüren.
</p>

<p>
Der 
<a href="https://debconf19.debconf.org/news/2019-07-20-open-day/">Tag der 
offenen Tür</a> am 20. Juli zog über 250 Leute an, denen viele für die 
Allgemeinheit interessante Vorträge und Workshops, eine Jobmesse mit Ständen 
mehrerer DebConf19-Sponsoren und ein Debian-Installierfest geboten wurden.
</p>

<p>
Die eigentliche Debian-Entwicklerkonferenz begann am Sonntag, dem 21. Juli 
2019. Neben den traditionellen ›Bits vom Projektleiter‹ (Bits from the DPL), 
Lightning Talks, Live-Demos und der Bekanntgabe der nächstjährigen DebConf 
(der <a href="https://wiki.debian.org/DebConf/20">DebConf20</a> in Haifa, 
Israel) drehten sich mehrere Sitzungen um Debian 10 Buster und einiger seiner 
neuen Funktionsmerkmale sowie um Neuigkeiten aus mehreren Projekten und 
den internen Teams. Darüber hinaus kam man zu Diskussionsrunden (BoFs) aus den 
Sprach-, Portierungs-, Infrastruktur- und den Community-Teams sowie zu 
Veranstaltungen über andere Themen rund um Debian und freie Software zusammen.
</p>

<p>
Der 
<a href="https://debconf19.debconf.org/schedule/">Veranstaltungskalender</a> 
wurde täglich mit geplanten und spontanen Aktivitäten vonseiten der Anwesenden 
ergänzt.
</p>

<p>
Für diejenigen, die nicht dabei sein konnten, wurden die meisten Vorträge 
und Sitzungen live gestreamt und aufgezeichnet. Die Aufzeichnungen sind auf 
der 
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2019/DebConf19/">Debian-Meetings-Archivwebsite</a> verfügbar. 
Daneben ermöglichten fast alle Veranstaltungen die Fernteilnahme per 
IRC-Nachrichten oder Online-Collaboration-Textdokumente.
</p>

<p>
Die <a href="https://debconf19.debconf.org/">DebConf19-Website</a> 
wird zu Archivierungszwecken aktiv bleiben und auch weiterhin Links und 
Videos zu den Vorträgen und Veranstaltungen vorhalten.
</p>

<p>
Die <a href="https://wiki.debian.org/DebConf/20">DebConf20</a> wird nächstes 
Jahr vom 23. bis 29. August 2019 in Haifa, Israel, stattfinden. Wie es 
die Tradition erfordert, werden die örtlichen Veranstalter die Konferenz 
mit dem DebCamp (16. bis 22. August) mit Fokus auf Individual- 
und Gruppenarbeiten zur Weiterentwicklung der Distribution eröffnen.
</p>

<p>
Der DebConf ist ein für alle Anwesenden sicheres und einladendes Umfeld 
wichtig. Deswegen halten sich während der Konferenz mehrere Teams 
(Empfang, Welcome Team und Anti-Harrassment-Team) bereit, um sowohl 
den Teilnehmerinnen und Teilnehmern vor Ort und online eine angenehme 
Anwesenheit zu ermöglichen als auch Lösungen für etwaige auftretende Probleme 
zu finden. 
Weitere Details dazu finden Sie auf der 
<a href="https://debconf19.debconf.org/about/coc/">DebConf19-Website unter Code of Conduct.</a>
</p>

<p>
Debian dankt den zahlreichen 
<a href="https://debconf19.debconf.org/sponsors/">Sponsoren</a> für ihre 
Unterstützung der DebConf19, vor allem unseren Platin-Sponsoren: 
<a href="https://www.infomaniak.com">Infomaniak</a>, 
<a href="https://google.com/">Google</a> 
und <a href="https://www.lenovo.com">Lenovo</a>.
</p>

<h2>Über Debian</h2>

<p>Das Debian-Projekt wurde 1993 von Ian Murdock als wirklich freies 
Gemeinschaftsprojekt gegründet. Seitdem ist das Projekt zu einem der größten 
und einflussreichsten Open-Source-Projekte angewachsen. Tausende von 
Freiwilligen aus aller Welt arbeiten zusammen, um Debian-Software herzustellen 
und zu betreuen. Verfügbar in über 70 Sprachen und eine große Bandbreite an 
Rechnertypen unterstützend, bezeichnet sich Debian als das <q>universelle 
Betriebssystem</q>.</p>

<h2>Über die DebConf</h2>

<p>
Die DebConf ist die Entwicklerkonferenz des Debian-Projekts. Neben einem 
mit technischen, sozialen und politischen Vorträgen gefüllten Zeitplan bietet 
die DebConf eine Möglichkeit für Entwicklerinnen, Unterstützer und andere 
Interessierte, sich persönlich zu treffen und enger zusammenzuarbeiten. Sie 
findet seit 2000 jedes Jahr an verschiedenen Orten wie Schottland, Argentinien 
und Bosnien-Herzegowina statt. Weitere Informationen zur DebConf sind unter 
<a href="https://debconf.org/">https://debconf.org</a> verfügbar.
</p>


<h2>Über Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> ist das größte 
Webhosting-Unternehmen der Schweiz. Es bietet unter anderem Backup- und 
Speicherlösungen sowie Lösungen für Veranstaltungen, Live-Streaming und 
Video-on-Demand-Dienste an. Alle seine Datencenter und sämtliche Elemente, 
deren Funktionieren für die Dienste und Produkte (sowohl Hard- als auch 
Software) unerlässlich sind, sind vollständig im Besitz der Informaniak.
</p>


<h2>Über Google</h2>

<p>
<a href="https://google.com/">Google</a> ist eines der größten Technik- 
Unternehmen der Welt; sein Angebot umfasst sehr viele verschiedene 
Internet-bezogene Dienste und Produkte wie Onlinewerbung, Suche, Cloud- 
Computing, Software und Hardware.
</p>

<p>
Google unterstützt Debian schon seit mehr als zehn Jahren als 
DebConf-Sponsor und betreibt darüber hinaus als Debian-Partner Teile der 
 <a href="https://salsa.debian.org">Salsa</a>-Continuous-Integration-Infrastruktur auf der Google-Cloud-Plattform.
</p>


<h2>Über Lenovo</h2>

<p>
Als ein globaler Technikanführer, der ein breites Portfolio von 
vernetzten Produkten wie Smartphones, Tablets, PCs und Workstations wie auch 
AR-/VR-Geräten herstellt, versteht <a href="https://www.lenovo.com">Lenovo,</a> 
wie kritisch offene Systeme und Plattformen für eine vernetzte Welt sind.
</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die DebConf19-Website unter 
<a href="https://debconf19.debconf.org/">https://debconf19.debconf.org/</a> 
oder senden eine E-Mail an &lt;press@debian.org&gt;.</p>
