#use wml::debian::translation-check translation="ea6a13e28f99e15f67ccfd6b74b4cc4bb185fdbd"
<define-tag pagetitle>Debian 9 actualizado: publicada la versión 9.5</define-tag>
<define-tag release_date>2018-07-14</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la quinta actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad,
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Corrección de errores varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction 2ping "Añade dependencia con python-pkg-resources, que faltaba">
<correction abiword "Resuelve conflicto de fichero binario entre abiword-dbgsym y abiword-plugin-grammar-dbgsym">
<correction adminer "No permite conexiones a puertos con privilegios [CVE-2018-7667]">
<correction animals "Corrige permisos incorrectos de fichero que hacían que el juego fuera inutilizable">
<correction apache2 "Actualiza mod_http y mod_proxy_http2 a las versiones a partir de la 2.4.33, corrigiendo fallos de segmentación, uso elevado de memoria y caída potencial [CVE-2018-1302]; hace que el script de inicialización de apache-htcacheclean use realmente /etc/default/apache-htcacheclean para su configuración">
<correction auto-complete-el "Añade corrección del proyecto original para emacs25; ajusta las dependencias de emacs a las versiones de emacs en stretch; establece auto-complete-el.emacsen-compat en «silencia aviso de instalación»">
<correction awffull "No usa en /etc/cron.daily/awffull opciones eliminadas">
<correction ax25-tools "Evita fallo de segmentación en tiempo de ejecución">
<correction base-files "Actualizado para esta versión">
<correction blktrace "Corrige desbordamiento de memoria en btt [CVE-2018-10689]">
<correction ca-certificates "Actualiza el lote de CA de Mozilla a la versión 2.22; correcciones de errores">
<correction camo "Añade dependencia con openssl, que faltaba">
<correction cffi "Añade ficheros que faltaban para cffi-libffi y para cffi-toolchain; añade varias dependencias que faltaban">
<correction check-postgres "Actualiza el juego de pruebas para tener en cuenta que pg_get_indexdef() no siempre incluye el nombre de esquema">
<correction clamav "Nueva versión del proyecto original; no falla por opciones de configuración eliminadas recientemente">
<correction clustershell "Añade dependencia con python-pkg-resources, que faltaba">
<correction debian-installer "Actualizado para la ABI -7 del kernel">
<correction debian-installer-netboot-images "Recompilado para esta versión">
<correction debian-security-support "Actualizados los datos incluidos">
<correction dehydrated "Corrige el fallo en la creación de fullchain.pem">
<correction devscripts "uscan: corrige la expresión regular de la versión del nuevo paquete en filenamemangle; debsign: corrige la finalización de bash; bts: soporta la nueva etiqueta <q>ftbfs</q>; uscan: soporta HTTPS en el redirector sf.net; debcheckout: soporta salsa.debian.org; debdiff: ordena los ficheros shlibs antes de comparar, reduciendo el ruido de diff; uscan: soporta realmente --copy">
<correction disc-cover "Corrige error de perl al ejecutar disc-cover">
<correction discover "Usa el tipo correcto para el parámetro length en la llamada a getline()">
<correction django-xmlrpc "Corrige dependencias de python3">
<correction dosbox "Corrige caídas con core=dynamic">
<correction dpdk "Nueva actualización del proyecto original «estable»">
<correction dpkg "Corrige desbordamiento de entero en el analizador sintáctico de la versión del formato de deb(5); corrige escalado de directorios con dpkg-deb --raw-extract; añade soporte de la CPU riscv64; no normaliza parámetros posteriores a una marca de fin de traspaso en Dpkg::Getopt; analiza sintácticamente de forma correcta nombres de usuario y nombres de grupo de start-stop-daemon que empiecen por dígitos; usa siempre la versión binaria del nombre del fichero .buildinfo">
<correction dput-ng "Añade objetivos jessie-backports-sloppy y stretch-backports; incluye «en pruebas» en las colecciones rm-managed y «antigua estable» en <q>protected distributions</q> («distribuciones protegidas»); añade el perfil ports-master; FTP: analiza sintácticamente y utiliza la parte opcional [:port] del fqdn">
<correction elastix "Recompilado con ITK, que, a su vez, se ha compilado con gcc 6">
<correction email2trac "Corrige detección de Trac 1.2">
<correction faad2 "Corrige varios problemas de denegación de servicio a través de ficheros MP4 manipulados [CVE-2017-9218 CVE-2017-9219 CVE-2017-9220 CVE-2017-9221 CVE-2017-9222 CVE-2017-9223 CVE-2017-9253 CVE-2017-9254 CVE-2017-9255 CVE-2017-9256 CVE-2017-9257]">
<correction faker "Añade dependencia con python-ipaddress, que faltaba">
<correction fastkml "Añade dependencia con pkg-resources, que faltaba">
<correction file "Evita la lectura más allá del fin de la zona de memoria [CVE-2018-10360]">
<correction freedink-dfarc "Corrige escalado de directorios en extractor D-Mod [CVE-2018-0496]">
<correction ganeti "Verifica correctamente certificados SSL al exportar máquinas virtuales">
<correction ghostscript "Corrige fallo de segmentación en gxht_thresh_image_init() con fichero aleatorizado; corrige desbordamiento de memoria en fill_threshold_buffer [CVE-2016-10317]; pdfwrite - Protección frente a intentos de escribir un número infinito [CVE-2018-10194]">
<correction git-annex "Correcciones de seguridad [CVE-2018-10857 CVE-2018-10859]">
<correction glx-alternatives "Nueva versión del proyecto original">
<correction gridengine "Usa rutas correctas de pixmaps de qmon; corrige FTBFS en armhf">
<correction intel-microcode "Actualiza el microcódigo incluido, e incorpora correcciones para Spectre v2 [CVE-2017-5715]">
<correction jdresolve "Corrige incompatibilidad con libnet-dns-perl en Debian 8 y posteriores">
<correction libb64 "Recompilado con PIE">
<correction libdate-holidays-de-perl "Marca el Día de la Reforma como festivo en Baja Sajonia y en Bremen">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libextractor "Varias correcciones de seguridad [CVE-2017-15266 CVE-2017-15267 CVE-2017-15600 CVE-2017-15601 CVE-2017-15602 CVE-2017-15922 CVE-2017-17440]">
<correction libipc-run-perl "Corrige filtración de memoria">
<correction liblouis "Corrige desbordamiento de memoria [CVE-2018-11410]; corrige varios desbordamientos de memoria [CVE-2018-11440 CVE-2018-11577 CVE-2018-11683 CVE-2018-11684 CVE-2018-11685 2018-12085]">
<correction libosmium "Escribe en la salida coordenadas con valor -2^31 correctamente; corrige zonas de memoria mayores de 2^32 bytes">
<correction linux "Nueva versión «estable» del proyecto original 4.9.110">
<correction linux-latest "Actualizado a la ABI -7 del kernel">
<correction llvm-toolchain-4.0 "Nuevo paquete para adaptaciones («backports») de rust; corrige compilación en s390x">
<correction local-apt-repository "Deja de romper apt cuando el paquete es eliminado («removed») pero su configuración no («not purged»)">
<correction loook "Corrige gestión de ficheros protegidos con contraseña">
<correction miniupnpd "Corrige denegación de servicio [CVE-2017-1000494]">
<correction nss-pam-ldapd "Aumenta el tamaño de la zona de memoria para el nombre de máquina («hostname»)">
<correction nvidia-graphics-drivers "Nueva versión del proyecto original">
<correction obfsproxy "No instala el perfil de AppArmor, que está roto">
<correction openldap "Corrige un problema de falta de sincronización con la replicación delta-syncrepl en entornos multi-master; corrige realmente las actualizaciones cuando la configuración contiene caracteres especiales precedidos por el carácter de escape contrabarra">
<correction openstack-debian-images "Pone CloudStack después de OpenStack en la lista de fuentes de datos («datasource_list») para evitar una espera de 120 segundos en cloud-init al iniciar una máquina en una nube OpenStack">
<correction patch "Corrige ejecución de órdenes arbitrarias en parches tipo ed [CVE-2018-1000156]">
<correction piglit "Corrige dependencia con python-mako, que faltaba">
<correction postgresql-9.6 "Nueva versión del proyecto original">
<correction postgresql-common "Impide que la actualización o el borrado de paquetes del servidor detenga clusters con otro número mayor de versión al ejecutar systemd">
<correction psad "Añade dependencias con net-tools y con iproute2, que faltaban">
<correction pysurfer "Añade dependencia con python-matplotlib, que faltaba">
<correction python-cluster "Añade dependencia con pkg-resources, que faltaba">
<correction python-pyorick "Corrige error de importación añadiendo dependencia con python3-numpy, que faltaba">
<correction python-scruffy "Añade dependencias con pkg-resources, que faltaban">
<correction r-cran-mi "Añade dependencia con r-cran-arm, que faltaba">
<correction redis "Corrige error tipográfico RunTimeDirectory -&gt; RuntimeDirectory en ficheros .service de systemd">
<correction reportbug "Notifica al equipo de seguridad o al equipo LTS sobre una posible regresión cuando se informe de un error contra un paquete que contenga una corrección de seguridad">
<correction rustc "Nueva versión del proyecto original para soportar Firefox ESR">
<correction salt "Corrige <q>salt-ssh minion configuración copiada desde Salt Master sin ajustar permisos</q> [CVE-2017-8109]">
<correction shared-mime-info "Cambia disparador de dpkg a noawait, corrigiendo problemas de actualización desde jessie">
<correction showq "Corrige el prefijo, de forma que la aplicación funcione realmente">
<correction source-highlight "Corrige dependencia con libboost-regex-dev">
<correction starplot "Corrige caída en el arranque">
<correction subversion "Rechaza commits que introducirían colisiones de hash con datos existentes, corrigiendo de esta forma el problema de SHA1/shattered («colisión SHA1»)">
<correction sus "Actualizado a una nueva versión, idéntica técnicamente a SUSv4 + TC1 + TC2">
<correction systemd "networkd-ndisc: gestiona la ausencia de MTU elegantemente; permite que se dé valor a RemoveIPC= en el fichero unit, no solo vía D-Bus; nspawn: añade -E a getopt_long, que faltaba; login: respeta --no-wall al cancelar una petición de apagado («shutdown»)">
<correction tclreadline "Corrige compilación de biblioteca compartida en ppc64el">
<correction thefuck "Añade dependencia con pkg-resources, que faltaba">
<correction tinyproxy "No deja de escuchar tras SIGHUP; corrige ruta de fichero de configuración; añade dependencia con adduser, que faltaba">
<correction tlslite-ng "Verifica el MAC incluso si el relleno tiene una longitud de un byte">
<correction tzdata "Nueva versión del proyecto original">
<correction unison "Recompilado con el ocaml de stretch">
<correction variety "Corrige inyección de intérprete de órdenes («shell injection») al borrar ficheros enviándolos a la papelera; corrige inyección de intérprete de órdenes («shell injection») en filter y clock con nombres de ficheros preparados de una manera determinada; blinda llamadas a ImageMagick contra inyecciones de intérprete de órdenes («shell injections») potenciales">
<correction xapian-core "Corrige MSet::snippet() para que incluya caracteres de escape HTML en todos los casos [CVE-2018-0499]">
<correction xerces-c "Corrige denegación de servicio a través de referencias a DTD externas [CVE-2017-12627]; corrige una regresión que hacía que gcc usara SSE2 incluso en plataformas que no lo soportan">
<correction xrdp "Corrige un error por uno («off-by-one») que podría dar lugar a caídas">
</table>

<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2017 4010 git-annex>
<dsa 2017 4064 chromium-browser>
<dsa 2018 4113 libvorbis>
<dsa 2018 4133 isc-dhcp>
<dsa 2018 4134 util-linux>
<dsa 2018 4135 samba>
<dsa 2018 4136 curl>
<dsa 2018 4137 libvirt>
<dsa 2018 4138 mbedtls>
<dsa 2018 4139 firefox-esr>
<dsa 2018 4140 libvorbis>
<dsa 2018 4141 libvorbisidec>
<dsa 2018 4142 uwsgi>
<dsa 2018 4143 firefox-esr>
<dsa 2018 4144 openjdk-8>
<dsa 2018 4145 gitlab>
<dsa 2018 4146 plexus-utils>
<dsa 2018 4148 kamailio>
<dsa 2018 4150 icu>
<dsa 2018 4151 librelp>
<dsa 2018 4152 mupdf>
<dsa 2018 4153 firefox-esr>
<dsa 2018 4155 thunderbird>
<dsa 2018 4156 drupal7>
<dsa 2018 4157 openssl>
<dsa 2018 4158 openssl1.0>
<dsa 2018 4159 remctl>
<dsa 2018 4160 libevt>
<dsa 2018 4161 python-django>
<dsa 2018 4162 irssi>
<dsa 2018 4163 beep>
<dsa 2018 4164 apache2>
<dsa 2018 4165 ldap-account-manager>
<dsa 2018 4167 sharutils>
<dsa 2018 4169 pcs>
<dsa 2018 4170 pjproject>
<dsa 2018 4171 ruby-loofah>
<dsa 2018 4172 perl>
<dsa 2018 4173 r-cran-readxl>
<dsa 2018 4174 corosync>
<dsa 2018 4175 freeplane>
<dsa 2018 4177 libsdl2-image>
<dsa 2018 4178 libreoffice>
<dsa 2018 4180 drupal7>
<dsa 2018 4181 roundcube>
<dsa 2018 4183 tor>
<dsa 2018 4184 sdl-image1.2>
<dsa 2018 4185 openjdk-8>
<dsa 2018 4188 linux>
<dsa 2018 4189 quassel>
<dsa 2018 4190 jackson-databind>
<dsa 2018 4191 redmine>
<dsa 2018 4192 libmad>
<dsa 2018 4193 wordpress>
<dsa 2018 4194 lucene-solr>
<dsa 2018 4195 wget>
<dsa 2018 4196 linux>
<dsa 2018 4197 wavpack>
<dsa 2018 4198 prosody>
<dsa 2018 4199 firefox-esr>
<dsa 2018 4200 kwallet-pam>
<dsa 2018 4201 xen>
<dsa 2018 4202 curl>
<dsa 2018 4203 vlc>
<dsa 2018 4203 phonon-backend-vlc>
<dsa 2018 4203 goldencheetah>
<dsa 2018 4206 gitlab>
<dsa 2018 4206 ruby-omniauth-auth0>
<dsa 2018 4207 packagekit>
<dsa 2018 4208 procps>
<dsa 2018 4209 thunderbird>
<dsa 2018 4210 xen>
<dsa 2018 4211 xdg-utils>
<dsa 2018 4212 git>
<dsa 2018 4213 qemu>
<dsa 2018 4214 zookeeper>
<dsa 2018 4215 batik>
<dsa 2018 4216 prosody>
<dsa 2018 4217 wireshark>
<dsa 2018 4218 memcached>
<dsa 2018 4219 jruby>
<dsa 2018 4220 firefox-esr>
<dsa 2018 4221 libvncserver>
<dsa 2018 4222 gnupg2>
<dsa 2018 4223 gnupg1>
<dsa 2018 4226 perl>
<dsa 2018 4227 plexus-archiver>
<dsa 2018 4228 spip>
<dsa 2018 4229 strongswan>
<dsa 2018 4230 redis>
<dsa 2018 4231 libgcrypt20>
<dsa 2018 4232 xen>
<dsa 2018 4233 bouncycastle>
<dsa 2018 4234 lava-server>
<dsa 2018 4235 firefox-esr>
<dsa 2018 4236 xen>
<dsa 2018 4238 exiv2>
<dsa 2018 4239 gosa>
<dsa 2018 4240 php7.0>
<dsa 2018 4241 libsoup2.4>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction libnet-whois-perl "Roto">
<correction mlbviewer "Ha dejado de funcionar debido a cambios del proveedor de contenidos">
<correction python-uniconvertor "No utilizable; necesita dependencias que no están empaquetadas">
<correction singularity-container "Sin posibilidad de soporte de seguridad">
<correction undertow "Sin posibilidad de soporte; varios problemas de seguridad; existen alternativas">
<correction visionegg "No utilizable; necesita numpy.oldnumeric, que ya no está disponible">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas por
esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>
