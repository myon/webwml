#use wml::debian::template title="Εγκαταστάτης του Debian" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="71179a1ce318116e9119dbdcac4acbdd17be5499" maintainer="galaxico"

<h1>Ειδήσεις</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Παλιότερες Ειδήσεις</a>
</p>

<h1>Εγκαθιστώντας με τον Εγκαταστάτη του Debian (Debian-Installer)</h1>

<p>
<if-stable-release release="stretch">
<strong>Για επίσημα μέσα <current_release_stretch> εγκατάστασης του Debian και 
πληροφορίες</strong>, δείτε τη 
<a href="$(HOME)/releases/stretch/debian-installer">σελίδα της stretch</a>.
</if-stable-release>
<if-stable-release release="buster">
<strong>Για επίσημα μέσα <current_release_buster> εγκατάστασης του Debian και 
πληροφορίες</strong>, δείτε τη
<a href="$(HOME)/releases/buster/debian-installer">σελίδα του buster</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
Όλοι οι σύνδεσμοι σε εικόνες αρχείων που ακολουθούν είναι για την έκδοση του 
Εγκαταστάτη του Debian που είναι υπό ανάπτυξη για την επόμενη έκδοση του Debian 
και θα εγκαταστήσει εξ ορισμού τη δοκιμαστική έκδοση 
του (<q><current_testing_name></q>).
</p>
</div>

<if-stable-release release="stretch">
<p>

<strong>Για να εγκαταστήσετε τη δοκιμαστική έκδοση του Debian</strong>, 
συνιστούμε την
 <strong><humanversion /></strong> έκδοση του εγκαταστάτη, αφού ελέγξετε 
τα 
<a href="errata">παροράματα</a>. Είναι διαθέσιμες οι ακόλουθες εικόνων 
αρχείων
<humanversion />:
<!--
<strong>To install Debian testing</strong>, we recommend you use
the <strong>daily builds</strong> of the installer. The following images are available for
daily builds:
-->
</p>

<h2>Επίσημη έκδοση</h2>

<div class="line">
<div class="item col50">
<strong>εικόνες CD netinst (γενικά 150-280 MB)</strong>
<netinst-images />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>CD (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>άλλες εικόνες (netboot, κλειδιά USB κλπ.)</strong>
<other-images />
</div>
</div>

<p>
Ή εγκαταστήσετε το <b>τρέχον εβδομαδιαίο στιγμιότυπο (snapshot) της Debian 
testing</b>
που χρησιμοποιεί την ίδια έκδοση του εγκαταστάτη όπως και η τελευταία 
έκδοση:
</p>

<h2>Τρέχοντα εβδομαδιαία στιγμιότυπα (snapshot)</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>CD (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>


</if-stable-release>
<!--
<p>
If you prefer to use the latest and greatest, either to help us test a future
release of the installer or because of hardware problems or other issues,
try one of these <strong>daily built images</strong> which contain the latest
available version of installer components.
</p>
-->

<h2>Τρέχοντα ημερήσια στιγμιότυπα (snapshot)</h2>

<div class="line">
<div class="item col50">
<strong>εικόνες CD netinst (γενικά 150-280 MB) <!-- and businesscard (generally 
20-50 MB) --> </strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>εικόνες CD netinst <!-- and businesscard --> (μέσω <a 
href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>εικόνες CD πολλαπλών αρχιτεκτονικών netinst</strong>
<devel-multi-arch-cd />
</div>

<div class="item col50 lastcol">
<strong>άλλες εικόνες (netboot, κλειδιά USB κλπ.)</strong>
<devel-other-images />
</div>
</div>

<p>
Αν οποιοδήποτε υλικό στο σύστημά σας <strong>απαιτεί τη φόρτωση υλισμικού 
(firmware)</strong> μέσω ενός οδηγού συσκευής, μπορείτε να χρησιμοποιήσετε ένα 
από τα <a 
href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/">
συμπιεσμένα αρχεία πακέτων κοινού υλισμικού</a>. Οδηγίες για το πώς να 
χρησιμοποιήσετε τα συμπιεσμένα αρχεία και γενικές πληροφορίες σχετικά με τη 
φόρτωση υλισμικού στη διάρκεια μιας εγκατάστασης μπορούν να βρεθούν στο Οδηγό 
Εγκατάστασης (δείτε την Τεκμηρίωση στη συνέχεια).
</p>

<p>
<strong>Σημειώσεις</strong>
</p>
<ul>
#	<li>Before you download the daily built images, we suggest you check for
#	<a href="https://wiki.debian.org/DebianInstaller/Today">known issues</a>.</li>
	<li>Μια αρχιτεκτονική μπορεί (προσωρινά) να παραλειφθεί από την επισκόπηση 
των ημερήσια δημιουργούμενων εικόνων αν αυτές δεν είναι (αξιόπιστα) 
διαθέσιμες.</li>
	<li>Για τις εικόνες αρχείων εγκατάστασης διατίθενται αρχεία επαλήθευσης 
(<tt>SHA256SUMS</tt>, <tt>SHA512SUMS</tt> και άλλα) στο ίδιον κατάλογο στον 
οποίο βρίσκονται οι αντίστοιχες εικόνες.</li>
	<li>Για τη μεταφόρτωση εικόνων ολόκληρων CD και DVD συνιστάται η χρήση του 
jigdo.</li>
	<li>Μόνο ένας περιορισμένος αριθμός εικόνων από τα σετ CD και DVD sets 
είναι διαθέσιμος ως αρχεία ISO για απευθείας μεταφόρτωση. Οι περισσότεροι 
χρήστες δεν χρειάζονται όλο το λογισμικό που βρίσκεται στο σύνολο των δίσκων, 
οπότε για να εξοικονομηθεί χώρος στους εξυπηρετητές μεταφόρτωσης και στους 
"καθρέφτες" των αρχειοθηκών του Debian, τα πλήρη σετ είναι διαθέσιμα μόνο 
μέσω jigdo.</li>
	<li>Η εικόνα <em>CD</em> πολλαπλών αρχιτεκτονικών υποστηρίζει τις 
αρχιτεκτονικές i386/amd64· η εγκατάσταση είναι παρόμοια με την εγκατάσταση από 
μια εικόνα netinst μιας μοναδικής αρχιτεκτονικής.</li>
	<li>Η εικόνα <em>DVD</em> πολλαπλών αρχιτεκτονικών υποστηρίζει τις 
αρχιτεκτονικές i386/amd64· η εγκατάσταση είναι παρόμοια με την εγκατάσταση από 
μια εικόνα ενός πλήρους CD μιας μοναδικής αρχιτεκτονικής· το DVD 
περιλαμβάνει επίσης τον πηγαίο κώδικα όλων περιεχόμενων πακέτων.</li>
</ul>

<p>
<strong>Μετά τη χρήση του Εγκαταστάτη του Debian</strong>, παρακαλούμε 
στείλτε μας μια 
<a
href="https://d-i.debian.org/manual/el.amd64/ch05s04.html#submit-bug">
αναφορά εγκατάστασης</a>,
ακόμα και στην περίπτωση που δεν υπήρξαν οποιαδήποτε προβλήματα.
</p>

<h1>Τεκμηρίωση</h1>

<p>
<strong>Αν πρόκειται να διαβάσετε ένα μοναδικό κείμενο </strong> πριν την 
εγκατάσταση, τότε διαβάστε το
<a href="https://d-i.debian.org/manual/el.amd64/apa.html">Howto της 
εγκατάστασης</a>, μια σύντομη περιήγηση στη διαδικασία της εγκατάστασης. 
Άλλη χρήσιμη τεκμηρίωση περιλαμβάνει:
</p>

<ul>
<li>Οδηγός Εγκατάστασης:
#    <a href="$(HOME)/releases/stable/installmanual">έκδοση της τρέχουσας 
#έκδοσης</a>
#    &mdash;
    <a href="$(HOME)/releases/testing/installmanual">υπό ανάπτυξη 
έκδοση (testing)</a>
    &mdash;
    <a href="https://d-i.debian.org/manual/">πιο πρόσφατη έκδοση (Git)</a>
<br />
λεπτομερείς οδηγίες εγκατάστασης</li>
<li><a 
href="https://wiki.debian.org/DebianInstaller/FAQ">Συχνές ερωτήσεις του 
Εγκαταστάτη του Debian</a>
and <a href="$(HOME)/CD/faq/">Συχνές ερωτήσεις των CD του Debian</a><br 
/>
κοινές ερωτήσεις και απαντήσεις</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Wiki του 
Εγκαταστάτη του Debian</a><br />
τεκμηρίωση συντηρούμενη από την κοινότητα</li>
</ul>

<h1>Επικοινωνήστε μαζί μας</h1>

<p>
Η  <a href="https://lists.debian.org/debian-boot/">λίστα 
αλληλογραφίας debian-boot</a> είναι το κύριο φόρμουμ για συζήτηση και δουλειά 
σχετικά με τον Εγκαταστάτη του Debian.
</p>

<p>
Έχουμε επίσης ένα κανάλι IRC, #debian-boot στον ιστότοπο 
<tt>irc.debian.org</tt>. Το κανάλι αυτό χρησιμοποιείται κυρίως για ανάπτυξη 
αλλά περιστασιακά και για υποστήριξη. Αν δεν πάρετε κάποια απάντηση, 
χρησιμοποιήστε εναλλακτικά τη λίστα.
</p>
