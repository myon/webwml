#use wml::debian::template title="Documentazione obsoleta"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"
#use wml::debian::translation-check translation="29ec0c5bdd03bc97fc7060abb3fee77ee6f66897" maintainer="Francesca Ciceri"
#first translator and maintainer="Johan Haggi"

<h1 id="historical">Documenti storici</h1>

<p>I documenti qui elencati furono scritti parecchio tempo fa e
e non sono stati aggiornati oppure sono stati scritti per versioni precedenti di Debian
e non sono stati aggiornati per la versione corrente. Il loro contenuto è quindi
superato, ma potrebbero avere ancora qualche utilità.</p>

<p>I riferimenti ai documenti che hanno perso la loro importanza e che non
hanno più alcuno scopo sono stati rimossi, tuttavia il codice sorgente di
parecchi dei manuali obsoleti possono essere trovati nell'<a
href="https://salsa.debian.org/ddp-team/attic">attivo del DDP</a>.</p>


<h2 id="user">Documentazione rivolta all'utente</h2>

<document "dselect Documentation for Beginners (Guida a dselect per principianti)" "dselect">

<div class="centerblock">
<p>
  Illustra dselect ai nuovi utenti ed il suo obiettivo è aiutare
  ad installare con successo Debian. Non cerca di spiegare
  tutto, cosicché quando si userà dselect per la
  prima volta, si dovrà consultare anche la guida in linea.
</p>
<doctable>
  <authors "Stéphane Bortzmeyer">
  <maintainer "(?)">
  <status>
  in stallo: <a href="https://packages.debian.org/aptitude">aptitude</a> ha
  rimpiazzato dselect come interfaccia standard Debian per la gestione dei pacchetti
  </status>
  <availability>
  <inddpcvs name="dselect-beginner" formats="html txt pdf ps"
            langs="ca cs da de en es fr hr it ja pl pt ru sk" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "User's Guide" "users-guide">

<div class="centerblock">
<p>
Questa <q>User's Guide</q> è solo una nuova formattazione di <q>Progeny User's Guide</q>.
I contenuti sono stati adattati al sistema standard Debian.</p>

<p>Più di 300 pagine con un buon tutorial per iniziare ad usare il sistema Debian
sia da <acronym lang="en" title="Graphical User Interface">GUI</acronym> che dalla linea di comando.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  Utile come tutorial. Scritto per la release Woody,
  sta divenendo obsoleta.
  </status>
  <availability>
# langs="en" isn't redundant, it adds the needed language suffix to the link
  <inddpcvs name="users-guide" index="users-guide" langs="en" formats="html txt pdf" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Tutorial" "tutorial">

<div class="centerblock">
<p>
Questo manuale è per i nuovi utilizzatori di Linux, per aiutarli a conoscere
Linux una volta installato, o per i nuovi utenti su un sistema Linux
amministrato da qualcun altro.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  in stallo; incompleto;
  probabilmente reso obsoleto da <a href="user-manuals#quick-reference">Debian Reference (La guida Debian)</a>
  </status>
  <availability>
  non ancora completato
  <inddpcvs name="debian-tutorial" cvsname="tutorial">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Guide to Installation and Usage" "guide">

<div class="centerblock">
<p>
  Un manuale rivolto agli utenti finali.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  pronto (ma è per potato)
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>
</div>

<hr />

<document "Debian User Reference Manual" "userref">

<div class="centerblock">
<p>
  Questo manuale fornisce le nozioni di base di tutto quello che un utente dovrebbe sapere
  circa il suo sistema Debian GNU/Linux (impostare X, configurare
  una rete, accedere ai floppy disk, ecc.). È pensato per colmare il gap
  tra il <q>Debian Tutorial</q> e le dettagliate pagine man ed info
  fornite con ogni pacchetto.</p>

  <p>È anche pensato per dare alcuni spunti su come combinare comandi e sul
  principio Unix secondo cui: <em>there is always more than one way to
  do it (c'è sempre più di un modo di farlo)</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  in stallo e abbastanza incompleto;
  probabilmente reso obsoleto da <a href="user-manuals#quick-reference">Debian Reference (La guida Debian)</a>
  </status>
  <availability>
  <inddpcvs name="user" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />


<document "Debian System Administrator's Manual" "system">

<div class="centerblock">
<p>
  Questo documento è menzionato nell'introduzione del <q>Policy manual</q>.
  Esso copre tutti gli aspetti dell'amministrazione di un sistema Debian.
</p>
<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "(?)">
  <status>
  in stallo; incompleto;
  probabilmente reso obsoleto da <a href="user-manuals#quick-reference">Debian Reference (La guida Debian)</a>
  </status>
  <availability>
  non ancora disponibile
  <inddpcvs name="system-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Network Administrator's Manual" "network">

<div class="centerblock">
<p>
  Questo manuale tratta tutti gli aspetti dell'amministrazione di rete di un sistema Debian.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  in stallo; incompleto;
  probabilmente reso obsoleto da <a href="user-manuals#quick-reference">Debian Reference (La guida Debian)</a>
  </status>
  <availability>
  non ancora disponibile
  <inddpcvs name="network-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

# Add this to books, there's a revised edition (2nd) @ Amazon
<document "The Linux Cookbook" "linuxcookbook">

<div class="centerblock">
<p>
  Una guida che mostra, con oltre 1.500 <q>ricette</q>, come usare Debian GNU/Linux
  nel lavoro quotidiano: dal lavoro con testi, immagini e suoni ai problemi di 
  rendimento e networking. Come il software che descrive, il libro è copylefted
  ed il sorgente è disponibile.
</p>
<doctable>
  <authors "Michael Stutz">
  <status>
  pubblicato; scritto per woody, sta divenendo obsoleto
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">from the author</a>
  </availability>
</doctable>
</div>

<document "APT HOWTO" "apt-howto">

<div class="centerblock">
<p>
  Questo manuale cerca di essere una fonte di informazione rapida ma 
  completa per il sistema APT e le sue caratteristiche. Contiene svariate
  informazioni sugli usi principali di APT e molti esempi.
</p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
  obsoleto dal 2009
  </status>
  <availability>
  <inpackage "apt-howto">
  <inddpcvs name="apt-howto" langs="en ca cs de es el fr it ja pl pt-br ru uk tr zh-tw zh-cn"
	    formats="html txt pdf ps" naming="locale" />
  </availability>
</doctable>
</div>


<h2 id="devel">Documentazione per gli sviluppatori</h2>

<document "Introduction: Making a Debian Package" "makeadeb">

<div class="centerblock">
<p>
  Introduzione su come creare un <code>.deb</code>, usando
  <strong>debmake</strong>.
</p>
<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  in stallo, reso obsoleto da <a href="devel-manuals#maint-guide">New Maintainers' Guide</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML online</a>
  </availability>
</doctable>
</div>

<hr />

<document "Debian Programmers' Manual" "programmers">

<div class="centerblock">
<p>
  Aiuta i nuovi sviluppatori a creare un pacchetto per il sistema Debian GNU/Linux.
</p>
<doctable>
  <authors "Igor Grobman">
  <status>
  reso obsoleto da <a href="devel-manuals#maint-guide">New Maintainers' Guide</a>
  </status>
  <availability>
  <inddpcvs name="programmer" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Packaging Manual" "packman">

<div class="centerblock">
<p>
  Questo manuale descrive gli aspetti tecnici della creazione di pacchetti Debian binari
  o di sorgenti. Documenta anche l'interfaccia tra dselect
  e gli script per i suoi metodi di accesso. Non tratta delle linee guida del Progetto
  Debian e presuppone una buona conoscenza del funzionamento di dpkg
  dal punto di vista dell'amministratore di sistema.

<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  Parti che erano le linee guida di fatto, sono state recentemente
  inserite nella <a href="devel-manuals#policy">debian-policy</a>.
  </status>
  <availability>
   <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<document "How Software Producers can distribute their products directly in .deb format" "swprod">

<div class="centerblock">
<p>
  Questo documento è inteso come punto di partenza per mostrare come i
  produttori di software possano integrare i loro prodotti in Debian, quali
  diverse situazioni possono derivare a seconda della licenza dei prodotti
  e delle scelte dei produttori, quali opportunità ci sono. Non spiega come
  creare i pacchetti ma elenca i documenti che lo fanno.
</p>

<p>
  Si dovrebbe leggerlo se non si conosce bene il complesso lavoro della
  creazione e distribuzione dei pacchetti Debian e, facoltativamente, quando
  li si aggiunge alla distribuzione Debian.
</p>

<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  obsoleto
  </status>
  <availability>
  <inddpsvn-distribute-deb>
  </availability>
</doctable>
</div>

<hr>

<h2 id="misc">Documentazione varia</h2>

<document "Debian Repository HOWTO" "repo">

<div class="centerblock">
<p>
  Questo documento spiega come funzionano i repository Debian, come crearli,
  come aggiungerli correttamente a <tt>sources.list</tt>.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  pronto (?)
  </status>
  <availability>
  <inddpcvs name="repository-howto" index="repository-howto"
            formats="html" langs="en fr de uk ta" vcstype="attic">
  </availability>
</doctable>
</div>

<document "Debian SGML/XML HOWTO" "sgml-howto">

<div class="centerblock">
<p>
  Questo HOWTO contiene informazioni pratiche sull'uso di SGML e XML
  con un sistema operativo Debian.
</p>

<doctable>
  <authors "Stephane Bortzmeyer">
  <maintainer "Stephane Bortzmeyer">
  <status>
  sospeso, obsoleto
  </status>
  <availability>
  <inddpcvs name="sgml-howto"
            formats="html"
            srctype="SGML"
            vcstype="attic"
            />
  </availability>
</doctable>
</div>
