msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-08-27 23:34+0700\n"
"Last-Translator: Izharul Haq <atoz.chevara@yahoo.com>\n"
"Language-Team: Debian Indonesia Translator <debian-l10n@debian-id.org>\n"
"Language: id_ID\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Amerika Serikat"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "Albania"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Armenia"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Argentina"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Austria"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Australia"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Bosnia Herzegovina"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Bangladesh"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "Belgia"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Bulgaria"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Brasil"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Bahama"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Belarusia"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Kanada"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Swiss"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Cili"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "Cina"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Kolombia"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Kostarika"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "Republik Ceko"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Jerman"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Denmark"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "Republik Dominika"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "Algeria"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Ekuador"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Estonia"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Mesir"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "Spanyol"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "Ethiopia"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Finlandia"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Kepulauan Faroe"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "Perancis"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Inggris"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Granada"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Georgia"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Greenland"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Yunani"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Guatemala"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Hongkong"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Honduras"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Kroasia"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Hongaria"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "Indonesia"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "Iran"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Irlandia"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Israel"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "India"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "Islandia"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Italia"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Jordania"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Jepang"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Kenya"

#: ../../english/template/debian/countries.wml:267
#, fuzzy
msgid "Kyrgyzstan"
msgstr "Kazakhstan"

#: ../../english/template/debian/countries.wml:270
msgid "Korea"
msgstr "Korea"

#: ../../english/template/debian/countries.wml:273
msgid "Kuwait"
msgstr "Kuwait"

#: ../../english/template/debian/countries.wml:276
msgid "Kazakhstan"
msgstr "Kazakhstan"

#: ../../english/template/debian/countries.wml:279
msgid "Sri Lanka"
msgstr "Sri Lanka"

#: ../../english/template/debian/countries.wml:282
msgid "Lithuania"
msgstr "Lituania"

#: ../../english/template/debian/countries.wml:285
msgid "Luxembourg"
msgstr "Luxemburg"

#: ../../english/template/debian/countries.wml:288
msgid "Latvia"
msgstr "Latvia"

#: ../../english/template/debian/countries.wml:291
msgid "Morocco"
msgstr "Maroko"

#: ../../english/template/debian/countries.wml:294
msgid "Moldova"
msgstr "Moldova"

#: ../../english/template/debian/countries.wml:297
msgid "Montenegro"
msgstr "Serbia Montenegro"

#: ../../english/template/debian/countries.wml:300
msgid "Madagascar"
msgstr "Madagaskar"

#: ../../english/template/debian/countries.wml:303
msgid "Macedonia, Republic of"
msgstr "Republik Dominika"

#: ../../english/template/debian/countries.wml:306
msgid "Mongolia"
msgstr "Mongolia"

#: ../../english/template/debian/countries.wml:309
msgid "Malta"
msgstr "Malta"

#: ../../english/template/debian/countries.wml:312
msgid "Mexico"
msgstr "Meksiko"

#: ../../english/template/debian/countries.wml:315
msgid "Malaysia"
msgstr "Malaysia"

#: ../../english/template/debian/countries.wml:318
msgid "New Caledonia"
msgstr "New Caledonia"

#: ../../english/template/debian/countries.wml:321
msgid "Nicaragua"
msgstr "Nikaragua"

#: ../../english/template/debian/countries.wml:324
msgid "Netherlands"
msgstr "Belanda"

#: ../../english/template/debian/countries.wml:327
msgid "Norway"
msgstr "Norwegia"

#: ../../english/template/debian/countries.wml:330
msgid "New Zealand"
msgstr "Selandia Baru"

#: ../../english/template/debian/countries.wml:333
msgid "Panama"
msgstr "Panama"

#: ../../english/template/debian/countries.wml:336
msgid "Peru"
msgstr "Peru"

#: ../../english/template/debian/countries.wml:339
msgid "French Polynesia"
msgstr "French Polynesia"

#: ../../english/template/debian/countries.wml:342
msgid "Philippines"
msgstr "Filipina"

#: ../../english/template/debian/countries.wml:345
msgid "Pakistan"
msgstr "Pakistan"

#: ../../english/template/debian/countries.wml:348
msgid "Poland"
msgstr "Polandia"

#: ../../english/template/debian/countries.wml:351
msgid "Portugal"
msgstr "Portugal"

#: ../../english/template/debian/countries.wml:354
msgid "Réunion"
msgstr ""

#: ../../english/template/debian/countries.wml:357
msgid "Romania"
msgstr "Rumania"

#: ../../english/template/debian/countries.wml:360
msgid "Serbia"
msgstr "Serbia"

#: ../../english/template/debian/countries.wml:363
msgid "Russia"
msgstr "Rusia"

#: ../../english/template/debian/countries.wml:366
msgid "Saudi Arabia"
msgstr "Arab Saudi"

#: ../../english/template/debian/countries.wml:369
msgid "Sweden"
msgstr "Swedia"

#: ../../english/template/debian/countries.wml:372
msgid "Singapore"
msgstr "Singapura"

#: ../../english/template/debian/countries.wml:375
msgid "Slovenia"
msgstr "Slovenia"

#: ../../english/template/debian/countries.wml:378
msgid "Slovakia"
msgstr "Slowakia"

#: ../../english/template/debian/countries.wml:381
msgid "El Salvador"
msgstr "El Salvador"

#: ../../english/template/debian/countries.wml:384
msgid "Thailand"
msgstr "Thailand"

#: ../../english/template/debian/countries.wml:387
msgid "Tajikistan"
msgstr "Tajikistan"

#: ../../english/template/debian/countries.wml:390
msgid "Tunisia"
msgstr "Tunisia"

#: ../../english/template/debian/countries.wml:393
msgid "Turkey"
msgstr "Turki"

#: ../../english/template/debian/countries.wml:396
msgid "Taiwan"
msgstr "Thailand"

#: ../../english/template/debian/countries.wml:399
msgid "Ukraine"
msgstr "Ukraina"

#: ../../english/template/debian/countries.wml:402
msgid "United States"
msgstr "Amerika Serikat"

#: ../../english/template/debian/countries.wml:405
msgid "Uruguay"
msgstr "Uruguay"

#: ../../english/template/debian/countries.wml:408
msgid "Uzbekistan"
msgstr "Uzbekistan"

#: ../../english/template/debian/countries.wml:411
msgid "Venezuela"
msgstr "Venezuela"

#: ../../english/template/debian/countries.wml:414
msgid "Vietnam"
msgstr "Vietnam"

#: ../../english/template/debian/countries.wml:417
msgid "Vanuatu"
msgstr "Vanuatu"

#: ../../english/template/debian/countries.wml:420
msgid "South Africa"
msgstr "Afrika Selatan"

#: ../../english/template/debian/countries.wml:423
msgid "Zimbabwe"
msgstr "Zimbabwe"

#~ msgid "Great Britain"
#~ msgstr "Inggris"
