#use wml::debian::template title="Debian buster -- 설치 안내" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#use wml::debian::translation-check translation="91954674647a3fceb1158db18ea7d17bc1093ce9" maintainer="Sebul"

<if-stable-release release="stretch">
<p>This is a <strong>beta version</strong> of the Installation Guide for Debian
10, codename buster, which isn't released yet. The information
presented here might be outdated and/or incorrect because of changes to
the installer. You might be interested in the
<a href="../stretch/installmanual">Installation Guide for Debian
9, codename stretch</a>, which is the latest released version of
Debian; or in the <a href="https://d-i.debian.org/manual/">Development
version of the Installation Guide</a>, which is the most up-to-date version
of this document.</p>
</if-stable-release>

<p>Installation instructions, along with downloadable files, are available
for each of the supported architectures:</p>

<ul>
<:= &permute_as_list('', 'Installation Guide'); :>
</ul>

<p>브라우저 지역화 속성을 설정했다면,  위 링크를 써서 바른 HTML 버전을 자동으로 볼 겁니다 &mdash;
<a href="$(HOME)/intro/cn">내용 협상</a>을 보세요.
Otherwise, pick the exact architecture, language, and format you want
from the table below.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architecture</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Languages</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   $_[2] eq html ? "$_[0].$_[1].$_[2]" : "$_[0].$_[2].$_[1]" } ); :>
</table>
</div>
