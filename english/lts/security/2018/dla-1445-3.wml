<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that the security update of busybox announced as
DLA-1445-1  to prevent the exploitation of <a href="https://security-tracker.debian.org/tracker/CVE-2011-5325">CVE-2011-5325</a>, a symlinking
attack, was too strict in case of cpio archives. This update restores
the old behavior.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:1.22.0-9+deb8u4.</p>

<p>We recommend that you upgrade your busybox packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1445-3.data"
# $Id: $
