<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in advancecomp, a collection
of recompression utilities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1056">CVE-2018-1056</a>

    <p>Joonun Jang discovered that the advzip tool was prone to a
    heap-based buffer overflow. This might allow an attacker to cause a
    denial-of-service (application crash) or other unspecified impact
    via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9210">CVE-2019-9210</a>

    <p>The png_compress function in pngex.cc in advpng has an integer
    overflow upon encountering an invalid PNG size, which results in
    another heap based buffer overflow.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.19-1+deb8u1.</p>

<p>We recommend that you upgrade your advancecomp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1702.data"
# $Id: $
