<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The urllib library in Python ships support for a second, not well known
URL scheme for accessing local files ("local_file://"). This scheme can
be used to circumvent protections that try to block local file access
and only block the well-known "file://" schema. This update addresses
the vulnerability by disallowing the "local_file://" URL scheme.</p>

<p>This update also fixes another regresssion introduced in the update
issued as DLA-1835-1 that broke installation of libpython3.4-testsuite.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.4.2-1+deb8u5.</p>

<p>We recommend that you upgrade your python3.4 packages.</p>

<p>For the detailed security status of python3.4 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python3.4">https://security-tracker.debian.org/tracker/python3.4</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1852.data"
# $Id: $
