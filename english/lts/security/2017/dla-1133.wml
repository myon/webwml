<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Ming:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11704">CVE-2017-11704</a>

    <p>Heap-based buffer over-read in the function decompileIF in util/decompile.c
    in Ming <= 0.4.8, which allows attackers to cause a denial of service via a
    crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11728">CVE-2017-11728</a>

    <p>Heap-based buffer over-read in the function OpCode (called from
    decompileSETMEMBER) in util/decompile.c in Ming <= 0.4.8, which allows
    attackers to cause a denial of service via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11729">CVE-2017-11729</a>

    <p>Heap-based buffer over-read in the function OpCode (called from
    decompileINCR_DECR line 1440) in util/decompile.c in Ming <= 0.4.8, which
    allows attackers to cause a denial of service via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11730">CVE-2017-11730</a>

    <p>Heap-based buffer over-read in the function OpCode (called from
    decompileINCR_DECR line 1474) in util/decompile.c in Ming <= 0.4.8, which
    allows attackers to cause a denial of service via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11731">CVE-2017-11731</a>

    <p>Invalid memory read in the function OpCode (called from isLogicalOp and
    decompileIF) in util/decompile.c in Ming <= 0.4.8, which allows attackers
    to cause a denial of service via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11734">CVE-2017-11734</a>

    <p>Heap-based buffer over-read in the function decompileCALLFUNCTION in
    util/decompile.c in Ming <= 0.4.8, which allows attackers to cause a denial of
    service via a crafted file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:0.4.4-1.1+deb7u4.</p>

<p>We recommend that you upgrade your ming packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1133.data"
# $Id: $
