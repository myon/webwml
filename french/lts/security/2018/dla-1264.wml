#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Ralph Dolmans et Karst Koymans ont découvert un défaut dans la manière
dont unbound validait les enregistrements NSEC synthétisés par caractère
générique. Un enregistrement NSEC incorrectement validé pourrait être
utilisé pour prouver la non existence (réponse NXDOMAIN) d'un
enregistrement à caractère générique existant, ou pour forcer unbound à
accepter une preuve NODATA.</p>

<p>Pour davantage d'informations veuillez consulter l'avertissement des
développeurs amont à l'adresse
<url "https://unbound.net/downloads/CVE-2017-15105.txt">.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.4.17-3+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets unbound.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1264.data"
# $Id: $
