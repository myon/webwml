#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10324">CVE-2016-10324</a>

<p>Dans libosip2 dans GNU oSIP 4.1.0, un message SIP mal formé peut conduire à
un dépassement de tampon basé sur le tas dans la fonction osip_clrncpy() définie
dans osipparser2/osip_port.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10325">CVE-2016-10325</a>

<p>Dans libosip2 dans GNU oSIP 4.1.0, un message SIP mal formé peut conduire à
un dépassement de tampon basé sur le tas dans la fonction _osip_message_to_str()
définie dans osipparser2/osip_message_to_str.c, aboutissant à un déni de service
distant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10326">CVE-2016-10326</a>

<p>Dans libosip2 dans GNU oSIP 4.1.0, un message SIP mal formé peut conduire à
un dépassement de tampon basé sur le tas dans la fonction osip_body_to_str()
définie dans osipparser2/osip_body.c, aboutissant à un déni de service
distant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7853">CVE-2017-7853</a>

<p>Dans libosip2 dans GNU oSIP 5.0.0, un message SIP mal formé peut conduire à
un dépassement de tampon basé sur le tas dans la fonction msg_osip_body_parse()
définie dans osipparser2/osip_message_parse.c, aboutissant à un déni de service
distant.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.6.0-4+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libosip2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-898.data"
# $Id: $
