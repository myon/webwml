#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans OpenSSL :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2177">CVE-2016-2177</a>

<p>Guido Vranken a découvert qu'OpenSSL utilise un pointeur arithmétique
non défini. Des informations supplémentaires peuvent être trouvées à
l'adresse <a href="https://www.openssl.org/blog/blog/2016/06/27/undefined-pointer-arithmetic/">
https://www.openssl.org/blog/blog/2016/06/27/undefined-pointer-arithmetic/</a>.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2178">CVE-2016-2178</a>

<p>Cesar Pereida, Billy Brumley et Yuval Yarom ont découvert une fuite de
temporisation dans le code de DSA.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2179">CVE-2016-2179</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-2181">CVE-2016-2181</a>

<p>Quan Luo et l'équipe d'audit OCAP ont découvert des vulnérabilités de
déni de service dans DTLS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2180">CVE-2016-2180</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-2182">CVE-2016-2182</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-6303">CVE-2016-6303</a>

<p>Shi Lei a découvert une lecture de mémoire hors limites dans
TS_OBJ_print_bio() et une écriture hors limites dans BN_bn2dec()
et dans MDC2_Update().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2183">CVE-2016-2183</a>

<p>Les suites de chiffrement basées sur DES sont rétrogradées du groupe
HIGH au groupe MEDIUM pour atténuer l'attaque SWEET32.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6302">CVE-2016-6302</a>

<p>Shi Lei a découvert que l'utilisation de SHA512 dans les tickets de
session TLS est vulnérable à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6304">CVE-2016-6304</a>

<p>Shi Lei a découvert qu'une requête d'état OCSP excessivement grande
peut avoir pour conséquence un déni de service par une surconsommation
de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6306">CVE-2016-6306</a>

<p>Shi Lei a découvert que l'absence de vérification de longueur de message
lors de l'analyse de certificats peut éventuellement avoir pour conséquence
un déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.0.1t-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl et
libssl1.0.0.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-637.data"
# $Id: $
